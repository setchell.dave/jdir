#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdarg.h>
#include "jdir.h"

struct ast *
newast(int nodetype, struct ast *l, struct ast *r) {
	struct ast *a = malloc(sizeof(struct ast));
	if (!a) { yyerror("outta space"); exit(0); }
	a->nodetype = nodetype;
	a->l = l;
	a->r = r;
	return a;
}

struct ast *
newstr(char *s) {
	struct stringtype *a = malloc(sizeof(struct stringtype));
	if (!a) { yyerror("outta space"); exit(0); }
	a->nodetype = 'S';
	a->value = malloc(strlen(s) + 1);
	if (!a->value) { yyerror("outta space"); exit(0); }
	strcpy(a->value, s);
	return (struct ast*)a;
}

struct ast *
newfloat(double f) {
	struct floattype *a = malloc(sizeof(struct floattype));
	if (!a) { yyerror("outta space"); exit(0); }
	a->nodetype = 'F';
	a->value = f;
	return (struct ast*)a;
}

struct ast *
newint(long i) {
	struct inttype *a = malloc(sizeof(struct inttype));
	if (!a) { yyerror("outta space"); exit(0); }
	a->nodetype = 'I';
	a->value = i;
	return (struct ast*)a;
}

struct ast *
newbool(short b) {
	struct booltype *a = malloc(sizeof(struct booltype));
	if (!a) { yyerror("outta space"); exit(0); }
	a->nodetype = 'B';
	a->value = b;
	return (struct ast*)a;
}

struct ast *
newmember(char *key, struct ast *v) {
	struct member *a = malloc(sizeof(struct member));
	if (!a) { yyerror("outta space"); exit(0); }
	a->nodetype = 'M';
	a->key = malloc(strlen(key) + 1);
	if (!a->key) { yyerror("outta space"); exit(0); }
	strcpy(a->key, key);
	a->value = v;
	return (struct ast*)a;
}

void
treefree(struct ast *a) {
	switch(a->nodetype) {
	case 'S':
		free( ((struct stringtype*)a)->value);
		break;
	case 'F': case 'I': case 'B':
		break;
	case 'M':
		free ( ((struct member *)a)->key);
		treefree( ((struct member *)a)->value);
		break;
	case 'L': case 'D':
		if ( a->l != NULL ) treefree(a->l);
		if ( a->r != NULL ) treefree(a->r);
		break;
	default: printf("failed to understand nodetype: %c\n", a->nodetype);
	}
	free(a);
}

int
eval(struct ast *a) {
	int retval = 0;
	if (!a) { yyerror("wtfbbq, null struct"); return 1; }
	switch (a->nodetype) {
	case 'S':
		printf("found a string \"%s\"\n", ((struct stringtype *)a)->value); break;
	}
	return retval;
}


void
yyerror(char *s, ...) {
	va_list ap;
	va_start(ap, s);
	fprintf(stderr, "%d: error: ", yylineno);
	vfprintf(stderr, s, ap);
	fprintf(stderr, "\n");
}

int
main() {
	printf("> ");
	return yyparse();
}
