%option nodefault noyywrap yylineno
%{
#include <stdlib.h>
#include "jdir.h"
#include "jdir.tab.h"
%}

QUOTE ["]
%%

"{" |
"}" |
"[" |
"]" |
":" |
"," { return yytext[0]; }

{QUOTE}{QUOTE} |
{QUOTE}[^"]+{QUOTE} { 
	yylval.s = malloc(strlen(yytext) + 1);
	if (!yylval.s) { yyerror("no space"); exit(0); }
	yytext[strlen(yytext)-1] = 0;
	strcpy(yylval.s, yytext+1); return STRING; }

[0-9]+"."[0-9] |
"."[0-9]+ { yylval.f = atof(yytext); return FLOAT; }

[0-9]+ { yylval.f = atoi(yytext); return INTEGER; }
[Tt][Rr][Uu][Ee] { yylval.b = 0; return TRUE; }
[Ff][Aa][Ll][Ss][Ee] { yylval.b = 1; return FALSE; }
[Nn][Uu][Ll][Ll] { yylval.b = -1; return JNULL; }

[[:space:]]+

%%
