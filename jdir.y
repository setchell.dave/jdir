%{
#include <stdio.h>
#include <math.h>
#include "jdir.h"
%}

%union {
	struct ast *a;
	long i;
	double f;
	char *s;
	short b;
}

%token <i> INTEGER
%token <f> FLOAT
%token <s> STRING
%token <b> TRUE FALSE JNULL

%type <a> jvalue object list lmembers members member
%nonassoc '[' '{'
%left ',' ':'
%start json

%%

json:
	| jvalue { eval($1); }
	;

jvalue: object
	| list
	| STRING { $$ = newstr($1); }
	| FLOAT { $$ = newfloat($1); }
	| INTEGER { $$ = newint($1); }
	| TRUE { $$ = newbool($1); }
	| FALSE { $$ = newbool($1); }
	| JNULL { $$ = newbool($1); }
	;

object: '{' '}' { $$ = newast('D', NULL, NULL); }
	| '{' members '}' { $$ = newast('D', $2, NULL); }
	;

list: '[' ']' { $$ = newast('L', NULL, NULL); }
	| '[' lmembers ']' { $$ = newast('L', $2, NULL); }
	;

members: member { $$ = newast('M', $1, NULL); }
	| member ',' members { $$ = newast('M', $1, $3); }
	;

member: STRING ':' jvalue { $$ = newmember($1, $3); }
	;

lmembers: jvalue { $$ = newast('A', $1, NULL); }
	| jvalue ',' lmembers { $$ = newast('A', $1, $3); }
	;

