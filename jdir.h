#ifndef JDIR_H
#define JDIR_H
#define NHASH 9997
extern int yylineno;
extern int yydebug;
void yyerror(char *s, ...);

struct ast {
	int nodetype;
	struct ast *l;
	struct ast *r;
};

struct inttype {
	int nodetype;
	long value;
};

struct floattype {
	int nodetype;
	double value;
};

struct stringtype {
	int nodetype;
	char *value;
};

struct booltype {
	int nodetype;
	short value;
};

struct member {
	int nodetype;
	char *key;
	struct ast *value;
};

struct ast *newmember(char *key, struct ast *);
struct ast *newast(int nodetype, struct ast *l, struct ast *r);

struct ast *newstr(char *s);
struct ast *newfloat(double f);
struct ast *newint(long i);
struct ast *newbool(short b);
int eval(struct ast *a);

#endif // JDIR_H